# unzoomify

![](https://img.shields.io/badge/written%20in-PHP-blue)

Find and combine images from a Zoomify tileset.

Zoomify is a format for splitting a high-resolution image into a set of small tiles, that can be browsed interactively via a web plugin. This project provides a command-line utility that will find and the tiles for a source image, download all the images (with progress bar) and merge them to reconstruct the source image. It must be supplied with a target URL and dimension parameters (that can be found via HTML inspection).

Tags: scraper

## See Also

- http://www.theatlantic.com/media/interactives/2015/11/colorado-satellite-photo/ A sample Zoomify image
- https://github.com/turban/Leaflet.Zoomify Open-source Zoomify viewer plugin for Leaflet.js
- https://github.com/lovasoa/dezoomify/ A similar project


## Download

- [⬇️ unzoomify-0.9.9.tar.xz](dist-archive/unzoomify-0.9.9.tar.xz) *(812B)*
